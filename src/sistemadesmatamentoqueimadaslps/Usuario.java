package sistemadesmatamentoqueimadaslps;

public class Usuario {
    
    private String nomeUsuario;
    private String senhaUsuario;
    
    Usuario(){
        this.nomeUsuario = "";
        this.senhaUsuario = "";
    }

    public String getNomeUsuario() {
        return nomeUsuario;
    }

    public void setNomeUsuario(String nomeUsuario) {
        this.nomeUsuario = nomeUsuario;
    }

    public String getSenhausuario() {
        return senhaUsuario;
    }

    public void setSenhaUsuario(String senhaUsuario) {
        this.senhaUsuario = senhaUsuario;
    }
    
    
    
}
